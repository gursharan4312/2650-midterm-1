import React, { Component } from 'react';

class Cars extends Component {

   deleteCar(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;

    /////////////////////////////////////////////////
    // STEP 15
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////

    carService.remove(id);


    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
   addCar(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;
    
    ///////////////////////////////////////
    // STEP 12
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////
   var car ={
     make : document.querySelector("#make").value,
     model: document.querySelector("#model").value,
     year: document.querySelector("#year").value,
     milage: document.querySelector("#milage").value
   }

  carService.create(car).then(result=>console.log(result))
    

    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { cars } = this.props;

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Cars</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addCar.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-4 mb-3">
                <label htmlFor="make">Make</label>
                <input type="text" className="form-control" id="make" defaultValue="" required />
                <div className="invalid-feedback">
                    A car make is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="model">Model</label>
                <input type="text" className="form-control" id="model" defaultValue="" required />
                <div className="invalid-feedback">
                    A car model is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="year">Year</label>
                <input type="number" className="form-control" id="year" defaultValue="2020" required />
                <div className="invalid-feedback">
                    A model year is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="model">Milage</label>
                <input type="text" className="form-control" id="milage" defaultValue="" required />
                <div className="invalid-feedback">
                    Milage is required.
                </div>
              </div>
              
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Milage</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {cars && cars.map(car => <tr key={car.id}>
            <th scope="row">{car.id}</th>
            <td>{car.make}</td>
            <td>{car.model}</td>
            <td>{car.year}</td>
            <td>{car.milage}</td>
            <td><button onClick={this.deleteCar.bind(this, car.id)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Cars;
