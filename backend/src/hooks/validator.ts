// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const {year,milage,make,model} = context.data;
    
    if(year>=1855 && year<=2020){
      if(milage>0){
        if(make!=="" && model !==""){
            context.data.make = context.data.make.substr(0,30);
            context.data.model = context.data.model.substr(0,30);
        }else throw Error("Enter Make and Model name")
      }else throw Error("Enter valid milage")
    }else throw new Error("Enter a valid year")
    
    return context;
  };
}
