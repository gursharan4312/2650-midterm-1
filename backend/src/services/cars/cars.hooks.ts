
import validator from '../../hooks/validator';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [validator()],
    update: [validator()],
    patch: [validator()],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
